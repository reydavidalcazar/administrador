
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries
  import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } 
  from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

  import { getStorage, ref, uploadBytesResumable, getDownloadURL }
   from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyDtxPBDGMVRNHA0-60Cq1QiHB1-JEWzAsw",
    authDomain: "proyectowebfinal-d8019.firebaseapp.com",
    databaseURL: "https://proyectowebfinal-d8019-default-rtdb.firebaseio.com",
    projectId: "proyectowebfinal-d8019",
    storageBucket: "proyectowebfinal-d8019.appspot.com",
    messagingSenderId: "590540270884",
    appId: "1:590540270884:web:33b12ea83846f6b3bc57b1"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase(app);
  const storage =getStorage();

  //yu6tfyt


  const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//declarar unas Variables global
var numSerie = 0;
var marca = "";
var modelo = "";
var descripcion = "";
var urlImag = "";

//Funciones
function leerInputs() {
    const numSerie = document.getElementById('txtNumSerie').value;
    const marca = document.getElementById('txtMarca').value;
    const modelo = document.getElementById('txtModelo').value;
    const descripcion = document.getElementById('txtDescripcion').value;
    const urlImag = document.getElementById('txtUrl').value;

    return { numSerie, marca, modelo, descripcion, urlImag };
}

function mostrarMensaje(mensaje) {
    const mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => { mensajeElement.style.display = 'none' }, 1000);
}

//Agregar productos a la DB
const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

function insertarProducto() {
    alert("Ingrese a add db");
    const { numSerie, marca, modelo, descripcion, urlImag } = leerInputs();
    //validar
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Faltaron Datos por Capturar");
        return;
    }
    set(
        refS(db, 'Automoviles/' + numSerie),
        {
            numSerie: numSerie,
            marca: marca,
            modelo: modelo,
            descripcion: descripcion,
            urlImag: urlImag
        }
    ).then(() => {
        alert("Se agrego con exito");
        Listarproductos();
    }).catch((error) => {
        alert("Ocurrio un error ")
    })
}

function limpiarInputs() {
    document.getElementById('txtNumSerie').value = '';
    document.getElementById('txtModelo').value = '';
    document.getElementById('txtMarca').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs(numSerie, marca, modelo, descripcion, urlImag) {
    document.getElementById('txtNumSerie').value = numSerie;
    document.getElementById('txtModelo').value = modelo;
    document.getElementById('txtMarca').value = marca;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
    const numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("No se ingreeso un Num de Serie");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
        if (snapshot.exists()) {
            const { marca, modelo, descripcion, urlImag } = snapshot.val();
            escribirInputs(numSerie, marca, modelo, descripcion, urlImag);
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con codigo " + numSerie + "No Existe.");
        }
    })
}

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

//Listar Productos

function Listarproductos() {
    const dbref = refS(db, 'Automoviles');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';
    onValue(dbref, (snapshot) => {
        snapshot.forEach(childSnapshot => {
            const childKey = childSnapshot.key;
            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = childKey;
            fila.appendChild(celdaCodigo);

            var celdaNombre = document.createElement('td');
            celdaNombre.textContent = data.marca;
            fila.appendChild(celdaNombre);

            var celdaPrecio = document.createElement('td');
            celdaPrecio.textContent = data.modelo;
            fila.appendChild(celdaPrecio);

            var celdaCantidad = document.createElement('td');
            celdaCantidad.textContent = data.descripcion;
            fila.appendChild(celdaCantidad);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImag;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    }, { onlyOnce: true });
}

Listarproductos();

//Funcion actualizar

function actualizarAutomovil() {
    const { numSerie, marca, modelo, descripcion, urlImag } = leerInputs();
    if (numSerie === "" || marca === "" || modelo === "" || descripcion === "") {
        mostrarMensaje("Favor de capturar toda la informacion");
        return;
    }
    alert("Actualizar");
    update(refS(db, 'Automoviles/' + numSerie), {
        numSerie: numSerie,
        marca: marca,
        modelo: modelo,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("Se actualizo con exito");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("Ocurrio un error: " + error);
    });
}

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarAutomovil);

//Funcion Borrar

function eliminarAutomovil() {
    const numSerie = document.getElementById('txtNumSerie').value.trim();
    if (numSerie === "") {
        mostrarMensaje("No se ingreso un Codigo Valido.");
        return;
    }

    const dbref = refS(db);
    get(child(dbref, 'Automoviles/' + numSerie)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'Automoviles/' + numSerie)).then(() => {
                mostrarMensaje("Producto eliminado con éxito.");
                limpiarInputs();
                Listarproductos();
            }).catch((error) => {
                mostrarMensaje("Ocurrio un error al eliminar el producto: " + error);
            });
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con ID " + numSerie + " no existe.");
        }
    });
}

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarAutomovil);

uploadButton.addEventListener('click',  (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if(file){
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on('state_changed', (snapshot) =>{
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            progressDiv.textContent = 'Progreso: ' + progress.toFixed(2) + '%';
        }, (error) => {
            console.error(error);
        }, () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() => {
                    progressDiv.textContent = '';

                }, 500);
            }).catch((error) =>{
                console.error(error);
            });
        });

    }
});
