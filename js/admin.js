

// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } 
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

import { getStorage, ref, uploadBytesResumable, getDownloadURL }
 from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDtxPBDGMVRNHA0-60Cq1QiHB1-JEWzAsw",
  authDomain: "proyectowebfinal-d8019.firebaseapp.com",
  databaseURL: "https://proyectowebfinal-d8019-default-rtdb.firebaseio.com",
  projectId: "proyectowebfinal-d8019",
  storageBucket: "proyectowebfinal-d8019.appspot.com",
  messagingSenderId: "590540270884",
  appId: "1:590540270884:web:33b12ea83846f6b3bc57b1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage =getStorage();


// declarar unas variables global
var numSerie =0;
var marca ="";
var modelo = "";
var descripcion = "";
var urlImag  = ""




// listar productos
// Listar Productos
Listarproductos();

function Listarproductos() {
    const section = document.getElementById("secAutos");

    const dbref = refS(db, 'Automoviles');

    onValue(dbref, (snapshot) => {
        const productosPorCategoria = {};

        snapshot.forEach(childSnapshot => {
            const data = childSnapshot.val();
            const marca = data.marca;

            if (!productosPorCategoria[marca]) {
                productosPorCategoria[marca] = [];
            }
            productosPorCategoria[marca].push(data);
        });

        section.innerHTML = '';

        for (const [marca, productos] of Object.entries(productosPorCategoria)) {
            section.innerHTML += `<hr><h2>${marca}</h2>`;

            productos.forEach(producto => {
                section.innerHTML += `<div class='card'>
                    <img id='urlImag' src='${producto.urlImag}' alt='' width='200px' height='200'>
                    <p id='marca' class='anta-regurar'>${producto.marca}</p>
                    <p id='marca' class='anta-regurar'>${producto.modelo}</p>
                    <p id='descripcion'>${producto.descripcion}</p>
                    <button>Agregar al carrito</button>
                </div>`;
            });
        }
    }, { onlyOnce: true });
}